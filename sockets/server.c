#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include "protocol.h"
#include <math.h>
#include <time.h>
#include <string.h>



int isBigEndian(){
	int one = 1;
	char* ptr;
	ptr = (char *)&one;
	return (*ptr);
}

uint64_t htond(double hostdouble) {
    char* data = (char*)&hostdouble;
    uint64_t result;
    char *dest = (char *)&result;
    int i =0;
    if(!isBigEndian()) {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[sizeof(double) - i - 1];
    } else {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[i];
    }
    return result;
}
double ntohd(uint64_t netdouble) {
    char* data = (char*)&netdouble;
    double result;
    char *dest = (char *)&result;
    int i =0;
    if(!isBigEndian()) {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[sizeof(double) - i - 1];
    } else {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[i];
    }
    return result;
}

int main (){
	int server_sockfd, client_sockfd;
	socklen_t client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;
	server_sockfd = socket (AF_INET, SOCK_STREAM, 0);

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl (INADDR_ANY);
	server_address.sin_port = htons (9734);
        
	bind (server_sockfd, (struct sockaddr *) &server_address, sizeof(server_address));
	listen (server_sockfd, 5);

	while (1){
		client_len = sizeof (client_address);
		client_sockfd = accept (server_sockfd, (struct sockaddr *) &client_address, &client_len);


		if (fork () == 0){
			///pierwiastek
			sqrtPack sqrtResponse;
			read (client_sockfd, &sqrtResponse, sizeof(sqrtPack));
			printf("%c %c %c %c\t %c\t%lf\n",sqrtResponse.header[0],sqrtResponse.header[1],sqrtResponse.header[2],sqrtResponse.header[3], sqrtResponse.requestId, ntohd(sqrtResponse.number));
			double temp = ntohd(sqrtResponse.number);			
			sqrtResponse.header[0] = '1';

			sqrtResponse.number = htond(sqrt(temp));
			write(client_sockfd,&sqrtResponse,sizeof(sqrtPack));
			
			///data i czas
			datePack dateResponse;
			read(client_sockfd,&dateResponse,sizeof(datePack));
			printf("%c %c %c %c\t %c\n",dateResponse.header[0],dateResponse.header[1],dateResponse.header[2],dateResponse.header[3], dateResponse.requestId);

			//czas systemu
			time_t mytime = time(NULL);
			const char* currentTime = ctime(&mytime);


			strcpy(dateResponse.date,currentTime);
			dateResponse.header[0] = '1';
			write(client_sockfd,&dateResponse,sizeof(datePack));
	
			close (client_sockfd);
			exit (0);
		}
		else{
			close (client_sockfd);
		}
	}
}

