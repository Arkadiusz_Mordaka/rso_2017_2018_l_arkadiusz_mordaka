#ifndef PROTOCOL_H
#define PROTOCOL_H

typedef struct sqrtPack {
    char header[4];
    char requestId;
    double number;
} sqrtPack;


typedef struct datePack {
    char header[4];
    char requestId;
    char date[32];
} datePack;

#endif
