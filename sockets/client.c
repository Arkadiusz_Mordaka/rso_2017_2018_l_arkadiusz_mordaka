#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "protocol.h"
#include <stdint.h>
#include <math.h>

int isBigEndian(){
	int one = 1;
	char* ptr;
	ptr = (char *)&one;
	return (*ptr);
}

uint64_t htond(double hostdouble) {
    char* data = (char*)&hostdouble;
    uint64_t result;
    char *dest = (char *)&result;
    int i =0;
    if(!isBigEndian()) {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[sizeof(double) - i - 1];
    } else {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[i];
    }
    return result;
}
double ntohd(uint64_t netdouble) {
    char* data = (char*)&netdouble;
    double result;
    char *dest = (char *)&result;
    int i =0;
    if(!isBigEndian()) {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[sizeof(double) - i - 1];
    } else {
        for ( i = 0; i < sizeof(double); i++)
            dest[i] = data[i];
    }
    return result;
}



int main (){
	int sockfd;
	struct sockaddr_in address;
	int result;

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (9734);

	result = connect (sockfd, (struct sockaddr *) &address, sizeof (address));





	if (result == -1){
		perror ("oops: netclient");
		exit (1);
	}

	double number = 0;
	printf("Podaj liczbe: ");
	scanf("%lf", &number);

	sqrtPack sqrtPackMessage;

	sqrtPackMessage.header[0] = '0';
	sqrtPackMessage.header[1] = '0';
	sqrtPackMessage.header[2] = '0';
	sqrtPackMessage.header[3] = '1';
	sqrtPackMessage.requestId = '1';
	sqrtPackMessage.number = htond(number);
	
	datePack datePackMessage;
	
	datePackMessage.header[0] = '0';
	datePackMessage.header[1] = '0';
	datePackMessage.header[2] = '0';
	datePackMessage.header[3] = '2';
	datePackMessage.requestId = '2';


	write(sockfd,&sqrtPackMessage,sizeof(struct sqrtPack));

	read (sockfd, &sqrtPackMessage, sizeof(struct sqrtPack));

        printf("%c %c %c %c\t %c\t %f\n",sqrtPackMessage.header[0],sqrtPackMessage.header[1],sqrtPackMessage.header[2],sqrtPackMessage.header[3],sqrtPackMessage.requestId, ntohd(sqrtPackMessage.number));

 
	write(sockfd,&datePackMessage,sizeof(struct datePack));
	
	read(sockfd,&datePackMessage,sizeof(struct datePack));
	printf("%c %c %c %c\t %c\t",datePackMessage.header[0],datePackMessage.header[1],datePackMessage.header[2],datePackMessage.header[3],datePackMessage.requestId);
        printf("%s\n", datePackMessage.date);

	close (sockfd);
	exit (0);

}
