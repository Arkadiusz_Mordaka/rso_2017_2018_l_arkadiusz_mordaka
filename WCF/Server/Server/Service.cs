﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server
{
    public class Service : IService
    {
        public static List<Item> library = new List<Item>()
        {
            new Item(new BookInfo("Ksiazka1","autor1")),
            new Item(new BookInfo("Ksiazka2","autor2")),
            new Item(new BookInfo("Ksiazka3","autor3")),
            new Item(new BookInfo("Ksiazka4","autor4")),
            new Item(new BookInfo("Ksiazka5","autor5")),
            new Item(new BookInfo("Ksiazka6","autor6")),
            new Item(new BookInfo("Ksiazka7","autor7")),
            new Item(new BookInfo("Ksiazka8","autor8")),
            new Item(new BookInfo("Ksiazka9","autor9"))
        };
        public List<Item> Library { get => library; set => library = value; }

        public bool addBook(Item book)
        {
            Library.Add(book);
            return true;
        }

        public Status borrowBook(int userId, int bookId)
        {
            Item item = getItem(bookId);
            if (item.BookInfo.Status.IsRented == false)
            {
                item.UserId = userId;
                item.ReturnDate = item.BookInfo.InternalReturnDate = new DateTime().AddDays(14);
                item.BookInfo.BorowDate = new DateTime();
                item.BookInfo.Status.IsRented = true;
                return item.BookInfo.Status;
            }
            else
                return new Status();


        }
        public Item getItem(int bookId)
        {
            var itemBook = library.Find(item => item.BookId == bookId);
            if (itemBook != null)
            {
                return itemBook;
            }
            else
            {
                return null;
            }
        }

        public List<Item> getAllBooks()
        {
            return Library;
        }

        public BookInfo getBookInfo(int bookId)
        {
            var itemBook = library.Find(item => item.BookId == bookId);
            if(itemBook != null)
            {
                return itemBook.BookInfo;
            }
            else
            {
                return new BookInfo();
            }
        }

        public List<Item> getBorrowedBooks(int userId)
        {
            var items = library.FindAll(item => item.UserId == userId);
            if (items != null)
            {
                return items;
            }
            else
            {
                return new List<Item>();
            }

        }

        public List<Item> listOfBorrowedItems()
        {
            var items = library.FindAll(item => item.BookInfo.Status.IsRented == true);
            if(items != null)
            {
                return items;
            }
            else
            {
                return new List<Item>();
            }
        }
    }
}
