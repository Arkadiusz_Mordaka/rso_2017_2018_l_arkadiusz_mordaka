﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server
{
    [ServiceContract]
    public interface IService
    {
       [OperationContract]
        BookInfo getBookInfo(int bookId);

        [OperationContract]
        List<Item> getBorrowedBooks(int userId);


        [OperationContract]
        List<Item> listOfBorrowedItems();

        [OperationContract]
        Status borrowBook(int userId, int bookId);

    }

    [DataContract]
    public class Item
    {
        static int counter;


        [DataMember]
        int bookId;

        [DataMember]
        int userId;

        [DataMember]
        DateTime? returnDate = null;

        [DataMember]
        BookInfo bookInfo;



        public Item(BookInfo bookInfo)
        {
            BookId = ++Counter;
            BookInfo = bookInfo;
        }

        public static int Counter { get => counter; set => counter = value; }
        public int BookId { get => bookId; set => bookId = value; }
        public int UserId { get => userId; set => userId = value; }
        public DateTime ReturnDate { get => (DateTime)returnDate; set => returnDate = value; }
        public BookInfo BookInfo { get => bookInfo; set => bookInfo = value; }

    }

    [DataContract]
    public class BookInfo
    {
        
        [DataMember]
        string title;

        [DataMember]
        string author;

        [DataMember]
        DateTime? borowDate = null;

        [DataMember]
        DateTime? internalReturnDate = null;

        [DataMember]
        Status status = new Status();



        public BookInfo()
        {

        }

        public BookInfo(string title, string author)
        {
            Title = title;
            Author = author;
        }

        public string Title { get => title; set => title = value; }
        public string Author { get => author; set => author = value; }
        public DateTime BorowDate { get => (DateTime)borowDate; set => borowDate = value; }
        public DateTime InternalReturnDate { get => (DateTime)internalReturnDate; set => internalReturnDate = value; }
        public Status Status { get => status; set => status = value; }
    }

    [DataContract]
    public class Status
    {
        [DataMember]
        bool isRented;

        public Status() { }
        public Status(bool rented)
        {
            IsRented = rented;
        }
        public bool IsRented { get => isRented; set => isRented = value; }
    }
}
