﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.ServiceReference;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceClient server = new ServiceClient();
            Console.WriteLine("-----BORROW BOOK -----");
            Console.WriteLine(server.borrowBook(1, 1).isRented);
            Console.WriteLine(server.borrowBook(1, 2).isRented);
            Console.WriteLine(server.borrowBook(2, 3).isRented);
            Console.WriteLine(server.borrowBook(2, 4).isRented);
            Console.WriteLine("-----TRY RENT BORROWED BOOK -----");
            Console.WriteLine(server.borrowBook(1, 3).isRented);


            Console.WriteLine("-----LIST OF BORROWED ITEMS -----");
            foreach (Item item in server.listOfBorrowedItems()){
                Console.WriteLine("BOOK ID: " + item.bookId + " " + item.bookInfo.author + " " + item.bookInfo.title + " Borrowed: " + item.bookInfo.borowDate + " Return Date: " + item.returnDate + " USER: " + item.userId);
            }

            Console.WriteLine("-----GET BOOK INFO -----");
            BookInfo book = server.getBookInfo(5);
            Console.WriteLine(book.author + " " + book.title);


            Console.WriteLine("-----GET BORROWED BOOKS (USER 1)-----");
            foreach (Item item in server.getBorrowedBooks(1))
            {
                Console.WriteLine("BOOK ID: " + item.bookId + " " + item.bookInfo.author + " " + item.bookInfo.title + " Borrowed: " + item.bookInfo.borowDate + " Return Date: " + item.returnDate);
            }

            Console.ReadLine();
        }
    }
}
