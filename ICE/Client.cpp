#include "Client.h"
#include <random>

namespace ClientApp {

    Client::Client(const string& name) : username(name) {
        try {
            iceCommunicator = Ice::initialize();
            Ice::ObjectPrx base = iceCommunicator->stringToProxy("Server:default -p 10000" );
            server = ServerPrx::checkedCast(base);
            if (!server)
                throw "Invalid proxy";
        } catch (const Ice::Exception& ex) {
            cerr << ex << endl;
        } catch (const char* msg) {
            cerr << msg << endl;
        }
        createUser();
    }


    void Client::createUser() {
        UserPtr object = new UserI(username);
        random_device rseed;
        mt19937 randomGenerator(rseed());
        uniform_int_distribution<> distribution(10001, 30000);
        adapter = iceCommunicator->createObjectAdapterWithEndpoints("User" + username, "default -p " + to_string(distribution(randomGenerator)));
        user = UserPrx::uncheckedCast(adapter->addWithUUID(object));
        adapter->activate();
    }



    Client::~Client() {
        for (auto &room : userRooms) {
            room->LeaveRoom(user);
        }
        if (iceCommunicator)
            iceCommunicator->destroy();
    }

    void Client::createRoom() const {
        scrollConsole();
        string roomName;
        cout << "Wpisz nazwe pokoju " << endl;
        cin >> roomName;
        cin.ignore(1000, '\n');
        try {
            server->CreateRoom(roomName);
        } catch (const Chat::RoomAlreadyExist& ex) {
            cerr << "BLAD! Pokoj taki istnieje" << endl;
        } catch (const Ice::UnknownException& ex) {
            cerr << "BLAD" << endl;
        }
        scrollConsole();
    }

    void Client::printListAllRooms() const {
        scrollConsole();
        auto rooms = server->getRooms();
        cout << "Lista pokoi: " << endl;
        for (auto room : rooms) {
            cout << room->getName() << endl;
        }
        cout << endl;
    }

    void Client::printAllUsers() const {
        scrollConsole();
        auto rooms = server->getRooms();
        cout << "Lista uzytkownikow: " << endl;
        for (auto room : rooms) {
            auto users = room->getUsers();
            for (auto& user : users) {
                cout << user->getName() << endl;
            }
        }
        cout << endl;
    }

    void Client::joinToRoom() {
        string name = getNameOfTheRoom();
        try {
            RoomPrx room = server->FindRoom(name);
            room->AddUser(user);
            userRooms.push_back(room);
        } catch (const NoSuchRoomExist& ex) {
            cerr << "Nie ma takiego pokoju" << endl;
        } catch (const UserAlreadyExists& ex) {
            cerr << "Uzytkownik taki istnieje" << endl;
        } catch (const Ice::UnknownException& ex) {
            cerr << "Blad" << endl;
        }
        scrollConsole();
    }

    void Client::printUsersInRoom() const {
        try {
            auto users = getUsersInRoom();
            scrollConsole();
            cout << "Uzytkownicy w pokoju:  " << endl;
            for (auto& user : users) {
                cout << user->getName() << endl;
            }
        } catch (const Ice::UnknownException& ex) {
            cerr << "BLAD" << endl;
        }
    }

    UserList Client::getUsersInRoom() const {
        string roomName = getNameOfTheRoom();
        try {
            RoomPrx room = server->FindRoom(roomName);
            UserList users = room->getUsers();
            return users;
        } catch (const NoSuchRoomExist& ex) {
            cerr << "Nie ma takiego pokoju" << endl;
        } catch (Ice::UnknownException& ex) {
            cerr << ex << endl;
        }
        return UserList();
    }

    string Client::getNameOfTheRoom() const {
        scrollConsole();
        string roomName;
        cout << "Wpisz nazwe pokoju " << endl;
        cin >> roomName;
        cin.ignore(1000, '\n');
        return roomName;
    }

    void Client::leaveRoom() {
        scrollConsole();
        string roomName = getNameOfTheRoom();
        for (auto roomsIterator =  userRooms.begin(); roomsIterator != userRooms.end(); ++roomsIterator) {
            if ((*roomsIterator)->getName() == roomName) {
                try {
                    (*roomsIterator)->LeaveRoom(user);
                    cout << "Opuszcza pokoj" << roomName << endl;
                    userRooms.erase(roomsIterator);
                    return;
                } catch (NoSuchUserExist& ex) {
                    cerr << "Blad" << endl;
                } catch (Ice::UnknownException& ex) {
                    cerr << ex << endl;
                }
            }
        }
        cerr << "Blad " << roomName << endl;
    }

    void Client::scrollConsole() const {
        for (unsigned int i = 0; i < 2; ++i) {
            cout << endl;
        }
    }

    void Client::sendPrivateMessageToUser() const {
        string targetUsername;
        UserList usersAvailable;
        auto rooms = server->getRooms();
        for (auto room : rooms) {
            auto users = room->getUsers();
            for (auto& user : users) {
                usersAvailable.push_back(user);
            }
        }
        cout << endl;
        cout << "Wpisz nazwe uzytkownika do ktorego chcesz napisac " << endl;
        cin >> targetUsername;
        cin.ignore(1000, '\n');
        for(auto& targetUser : usersAvailable) {
            if (targetUser->getName() == targetUsername) {
                string message;
                cout << "Wpisz wiadomosc: " << endl;
                getline(cin, message);
                targetUser->SendPrivateMessage(user, message);
                return;
            }
        }
        cerr << "Nie znaleziono uzytkownika" << endl;
    }

    void Client::sendMessageToRoom() const {
        scrollConsole();
        string targetRoom = getNameOfTheRoom();
        for (auto roomsIterator =  userRooms.begin(); roomsIterator != userRooms.end(); ++roomsIterator) {
            if ((*roomsIterator)->getName() == targetRoom) {
                try {
                    string content;
                    cout << "Wpisz wiadomosc" << endl;
                    getline(cin, content);
                    (*roomsIterator)->SendMessage(user, content);
                    return;
                } catch (NoSuchUserExist& ex) {
                    cerr << "Blad!" << endl;
                    return;
                }
            }
        }
        cerr << "Blad" << targetRoom << endl;
    }
}