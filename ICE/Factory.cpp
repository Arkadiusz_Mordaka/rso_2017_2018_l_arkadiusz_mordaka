#include "Factory.h"
#include <random>

namespace ChatApp {
    void Factory::registerRoomFactory() {
        RoomFactoryPtr object = new RoomFactoryI();
        random_device rseed;
        mt19937 randomGenerator(rseed());
        uniform_int_distribution<> distribution(10001, 30000);
        adapter = iceCommunicator->createObjectAdapterWithEndpoints("RoomFactory", "default -p " + to_string(distribution(randomGenerator)));
        roomFactory = RoomFactoryPrx::uncheckedCast(adapter->addWithUUID(object));
        adapter->add(object, iceCommunicator->stringToIdentity("RoomFactory"));
        adapter->activate();
        server->RegisterRoomFactory(roomFactory);
        iceCommunicator->waitForShutdown();
    }

    Factory::Factory() {
        try {
            iceCommunicator = Ice::initialize();
            Ice::ObjectPrx base = iceCommunicator->stringToProxy("Server:default -p 10000");
            server = ServerPrx::checkedCast(base);
            if (!server) {
                throw "Invalid proxy";
            }
        } catch (const RoomAlreadyExist &ex) {
            cerr << ex << endl;
        } catch (const NoSuchRoomExist& ex) {
            cerr << ex << endl;
        } catch (const UserAlreadyExists& ex) {
            cerr << "Such userr already exist" << endl;
        } catch (const Ice::Exception& ex) {
            cerr << ex << endl;
        } catch (const char* msg) {
            cerr << msg << endl;
        }
    }

    Factory::~Factory() {
        server->UnregisterRoomFactory(roomFactory);
    }
}