#include "RoomFactoryI.h"

namespace ChatApp {
    RoomList RoomFactoryI::getRooms(const ::Ice::Current&) {
        return roomList;
    }

    RoomPrx RoomFactoryI::createRoom(const string& name, const ::Ice::Current&) {
        RoomPtr object = new RoomI(name);
        random_device rseed;
        mt19937 randomGenerator(rseed());
        uniform_int_distribution<> distribution(10001, 30000);
        int port = distribution(randomGenerator);
        adapter = ic->createObjectAdapterWithEndpoints("SimpleRoom" + name, "default -p " + to_string(port));
        adapter->add(object, ic->stringToIdentity("SimpleRoom" + name));
        adapter->activate();
        Ice::ObjectPrx base = ic->stringToProxy("SimpleRoom" + name + ":default -p " + to_string(port));
        RoomPrx room = RoomPrx::checkedCast(base);
        roomList.push_back(room);
        cout << "Room created" << endl;
        return room;
    }

    RoomFactoryI::RoomFactoryI() {
        ic = Ice::initialize();
    }

    RoomFactoryI::~RoomFactoryI() {
        if (ic) {
            try {
                ic->destroy();
            } catch (const Ice::Exception& e) {
                cerr << e << endl;
            }
        }
    }
}