#include "Factory.h"

using namespace ChatApp;

int main(int argc, char* argv[]) {
    Factory factory;
    factory.registerRoomFactory();

    return 0;
}