#include "Menu.h"

namespace ClientApp {
    void Menu::printMenu() {
        cout << endl;
        cout << "---------MENU---------" << endl;
        cout << "1. Stworz pokoj" << endl;
        cout << "2. Wyswietl dostepne pokoje" << endl;
        cout << "3. Dolacz do pokoju" << endl;
        cout << "4. Opusc pokoj" << endl;
        cout << "5. Wyslij wiadomosc do pokoju" << endl;
        cout << "6. Wywswietl liste uzytkownikow w pokoju" << endl;
        cout << "7. Wyslij prywatna wiadomosc" << endl;
        cout << "8. Wyswietl wszystkich aktywnych uzytkownikow" << endl;
        cout << "0. Wyjdz" << endl;
        cout << endl;
    }

    void Menu::getInput() {
        cin >> choice; 
        while (choice < 0 || choice > 8) {
            cout << "Niepoprawna komenda!" << endl;
            cin >> choice;
        }
    }

    int Menu::getChoice() {
        getInput();
        return choice;
    }
}